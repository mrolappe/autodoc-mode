;;; autodoc-mode.el --- Major mode for browsing Amiga AutoDoc files -*- lexical-binding: t -*-

;; Author: marco rolappe
;; Maintainer: marco rolappe
;; Version: 0.2
;; Homepage: https://codeberg.org/mrolappe/autodoc-mode
;; Keywords: docs


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Major mode for browsing Amiga AutoDoc files

;; TODO? dim captions of empty sections

;;; Code:

(defface autodoc-toc-caption
    '((t :weight bold :height 1.2))
    "This face defines the look of the TOC caption.")

(defface autodoc-toc-link
    '((t :inherit link))
    "This face defines the look of TOC entries rendered as links.")

(defface autodoc-section-caption
    '((t :weight bold :height 1.1))
    "This face defines the look of section captions of an entry.")

(defun autodoc-mode/init-keymap ()
    "Initialize mode's keymap."

    (let ((map autodoc-mode-map))
        (define-key map "t" 'autodoc-mode/goto-toc)
        (define-key map "n" 'autodoc-mode/goto-next-entry)
        (define-key map "p" 'autodoc-mode/goto-prev-entry)
        (define-key map "g" 'autodoc-mode/goto-entry)))

(defun autodoc-mode/goto-toc ()
    "Go to the table of contents, i.e. the beginning of the buffer."
    (interactive)

    (goto-char (point-min)))

(defun autodoc-mode/propertize-toc ()
    (goto-char (point-min))

    (when (looking-at "^\\(TABLE OF CONTENTS\\)[^\f]*\f")
        (put-text-property (match-beginning 1) (match-end 1) 'font-lock-face 'autodoc-toc-caption)
        (put-text-property (match-beginning 0) (match-end 0) 'autodoc-current "TOC")))

(defun autodoc-mode/goto-entry (name)
    "Go to the entry given by NAME."
    (interactive)
    
    (let ((entry-pos
           (save-excursion
               (goto-char (point-min))

               (when (search-forward "\f" nil t) ;; end of TOC
                   (backward-char) ;; so that entry RE below can match first TOC entry
                   (re-search-forward (rx "\f" (literal name)))))))

        (when entry-pos
            (goto-char entry-pos))))

(defvar autodoc-mode/toc-link-map
    (let ((map (make-sparse-keymap)))
        (define-key map [mouse-2] 'autodoc-mode/on-toc-entry-click)
        map))

(defun autodoc-mode/on-toc-entry-click (event)
    "Called when a TOC entry is clicked."
    (interactive "e")

    (let ((target (get-text-property (posn-point (event-end event)) 'autodoc-target)))
        (autodoc-mode/goto-entry target)))

(defun autodoc-mode/linkify-toc-entries ()
    "Convert TOC entries to links for jumping to the respective entries."
    (interactive)

    (save-excursion
        (goto-char (point-min))

        (let ((end-of-toc (re-search-forward (rx "\f") nil t)))
            (unless end-of-toc
                (error "End of TOC not found"))

            (goto-char (point-min))

            (while (re-search-forward "^\\([^ \n]+?\\)/\\([^ ]+?\\)$" end-of-toc t)
                (add-text-properties (match-beginning 0) (match-end 0)
                                     `( keymap ,autodoc-mode/toc-link-map
                                        help-echo ,(concat "Go to entry: " (match-string-no-properties 0))
                                        autodoc-target ,(match-string-no-properties 0)
                                        font-lock-face autodoc-toc-link
                                        follow-link 'mouse-face
                                        mouse-face highlight))))))

(defun autodoc-mode/fontify-buffer ()
    "Fontify the buffer."

    (save-excursion
        (goto-char (point-min))

        (while (and (re-search-forward
                     "^ +\\(BUGS\\|EXAMPLES?\\|EXCEPTIONS\\|FUNCTION\\|INPUTS\\|METHODS\\|NAME\\|NOTES?\\|RESULTS?\\|RETURNS\\|SEE ALSO\\|SYNOPSIS\\|TAGS\\|WARNING\\)$" nil t)
                    (not (eobp)))
            (put-text-property (match-beginning 1) (match-end 1) 'font-lock-face 'bold))))

;;;###autoload
(define-derived-mode autodoc-mode text-mode "AutoDoc"
    "Major mode for browsing Amiga AutoDoc files."
    :group 'autodoc

    (autodoc-mode/init-keymap)

    (set (make-local-variable 'imenu-generic-expression)
         '((nil "\f\\([^ ]+/[^ ]+\\) +\\1" 1)))

    (autodoc-mode/propertize-toc)
    (autodoc-mode/linkify-toc-entries)
    (autodoc-mode/fontify-buffer)

    (setq buffer-read-only t
          ;; tab-width 4
          header-line-format '("Current: " (:eval (propertize (or (get-text-property (point) 'autodoc-current)
                                                                  "unknown")
                                                              ;; 'help-echo "TODO"
                                                              )))))

(provide 'autodoc-mode)

;;; autodoc-mode.el ends here
